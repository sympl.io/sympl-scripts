# Sympl Scripts

A collection of scripts and tool to do somewhat common tasks within Sympl.

These scripts are currently in Beta and may have missing features or fail to function as expected.

Please report any problems (or success) on the [Sympl Forum](https://forum.sympl.host).